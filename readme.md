"How to Make Embedded Acoustic Instruments"
===========================================

A summary of:  

- Berdahl, E. (2014). How to Make Embedded Acoustic Instruments. In Proceedings of the International Conference on New Interfaces for Musical Expression (pp. 140–143). London, United Kingdom. Retrieved from http://www.nime.org/proceedings/2014/nime2014_551.pdf

## Overview: 

> "An _embedded acoustic instrument_ is an __embedded musical instrument__ that provides __direct sound output__."

The paper describes basic features of an embedded acoustic instrument (EAI), gives tips and instructions for building one, and gives an example with the LapBox. 

### Advantages of embedded instruments: 

- Single purpose-built instruments and interfaces, especially for long-running installations, etc.
- Low cost
- Less maintenance
- Fewer cables

### Platforms for building embedded acoustic instruments: 

- [Satellite CCRMA](https://ccrma.stanford.edu/~eberdahl/Satellite/)
    - Berdahl, E., & Ju, W. (2011). Satellite CCRMA: A Musical Interaction and Sound Synthesis Platform. In Proceedings of the International Conference on New Interfaces for Musical Expression (pp. 173–178). Oslo, Norway. Retrieved from http://www.nime.org/proceedings/2011/nime2011_173.pdf
- [Bela](https://bela.io/)
    - McPherson, A. P., & Zappi, V. (2015). An environment for submillisecond-latency audio and sensor processing on beaglebone black. In 138th Audio Engineering Society Convention (pp. 965–971). Warsaw, Poland.
    - Morreale, F., Moro, G., Chamberlain, A., Benford, S., & McPherson, A. P. (2017). Building a maker community around an open hardware platform. Conference on Human Factors in Computing Systems - Proceedings, 2017-May, 6948–6959. https://doi.org/10.1145/3025453.3026056
- [Prynth](https://prynth.github.io)
    - de Almeida Soares Franco, I. (2019). A Framework for Embedded Digital Musical Instruments. Dissertation. McGill University.

### Question: 

**How to provide an embedded instrument with a direct, cleanly controllable acoustic output?** 

---------

## Embedded Acoustic Instruments (EAIs)

<div style="text-align:center"><img src="images/image1.jpg" alt="figure 1" width="50%"></div>

**Basic structural elements of an EAI and operational flow:**

1. Sensors
    - Measure physical interactions
2. Sensor interface (microcontroller - Arduino Teensy, etc.)
    - Converts incoming sensor signals to numerical data, sends to SBC
3. Embedded computation (SBC - Raspberry Pi, BeagleBone, etc.)
    - sound synthesis and processing
    - receives sensor data and maps to synthesis parameters
    - outputs audio signal
4. Audio amplifier
    - amplifies audio signal to power transducer
5. Internal transducer (loudspeaker)
    - outputs sound at audible volume
6. Enclosure (instrument housing)
    - contains all components
    - can can support sound radiation based on shape/size/material

(There are, of course, other configurations - eg. microcontrollers with onboard/added audio capabilities and don't require additional computation, or sensor acquisition directly on the SBC as with the Bela system).

### Radiation Patterns of instruments: 

- Radiation patterns of acoustic instruments are very complex, and depend on shape, size, material, actuation method, etc.
- When designing instruments with acoustic sound output, must consider the same, especially when working with ensembles
- Laptop ensembles have used multidirectional speaker arrays to create different radiation patterns
- Also mentioned: ***actuated acoustic instruments***

> "However, the focus of embedded acoustic instruments lies in being able to **create a wide range of timbres** that would ideally be **unrestricted by the enclosure geometry**. This is why embedded acoustic instruments typically actuate sound waves directly **using loudspeaker-type transducers**."

-----------

## Building EAIs

Paper gives instruction for constructing an instrument with a laser cut enclosure. 

**Challenge:** eliminate buzzing and rattling of enclosure pieces and mounted components due to naturally occurring structural resonances.

Can identify and eliminate unwanted rattling by playing a sine tone through the speaker(s), then sweeping the frequency to find structural resonances. 

Tips to eliminate: 

- use sufficiently thick panels (suggest at least 5mm)
- install loudspeakers on faces with least area
- the smaller the enclosure the better
- non-integer relationships between sides
- install a brace to dampen vibrations (see image)
- Place foam between components and enclosure if necessary.
- Move/fix rattling/loose wires.
- Use gaskets for loudspeaker mounting. 

<div style="text-align:center"><img src="images/image7.jpg" alt="figure 1" width="50%"></div>

## Example instrument: The LapBox

<div style="text-align:center"><img src="images/image5.jpg" alt="figure 1" width="50%"></div>

## Resources: 

- [Edgar Berdahl's Embedded Acoustic Instruments page](`https://ccrma.stanford.edu/~eberdahl/EAI/index.html) 
- [How to build a powered loudspeaker](https://ccrma.stanford.edu/~eberdahl/EAI/HowToBuildAPoweredLoudspeaker.pdf)






